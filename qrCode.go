// Copyright 2021 almaember

// This file is part of PCRemote.
//
// PCRemote is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PCRemote is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PCRemote.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"os"

	"github.com/mdp/qrterminal/v3"
)

func displayQRCode(ip string) {
	url := fmt.Sprintf("http://%s:3467/index.html", ip)

	qrterminal.Generate(url, qrterminal.L, os.Stdout)
}
