# PCRemote
## What is PCRemote?
PCRemote is a simple command-line tool to use your phone as a remote for your computer, for example to step presentations without having to stand next to it.

The client UI features a nice, dark color scheme so you don't go blind even in a dark room.

## Technical details
The server part of PCRemote is written in [Golang][1]. It also uses some JavaScript to implement the client. The client uses no frameworks or libraries.

The Golang tool launches a simple HTTP server that serves the files embedded into the executable (namely the JavaScript, HTML and CSS files). It also launches a WebSocket server that allows the client to report keypresses in real time.

[1]: https://golang.org

## Usage
### Installation
Currently, there are no binaries available. For this reason, you will have to build PCRemote from source yourself, the instructions for which will be detailed below.

Please note that the following instructions are primarily for Linux systems, and may not work on non-Linux ones.

* *Step 1:* install the required dependencies
Right now, you will have to install [Golang][1], [Git][3], and the [dependencies for RobotGo][2].

Providing installation instructions for all of these would be hard, so please use the instruction on their websites.

However, for Ubuntu, you can install them all with these commands:

```sh
sudo apt install gcc libc6-dev

sudo apt install libx11-dev xorg-dev libxtst-dev libpng++-dev

sudo apt install xcb libxcb-xkb-dev x11-xkb-utils libx11-xcb-dev libxkbcommon-x11-dev
sudo apt install libxkbcommon-dev

sudo apt install xsel xclip

sudo apt install git golang
```

Alternatively, you can skip installing Git and download the source as an archive from GitLab, but I do not recommend you do it this way, because it will make it significantly harder to update in the future.

[2]: https://github.com/go-vgo/robotgo#requirements
[3]: https://git-scm.org

* *Step 2:* download the sources

First, navigate to the directory where you want to download the sources:

```sh
cd ~/Documents/src
```
Then, download the sources using Git and move into the source directory:

```sh
git clone https://gitlab.com/almaember/pcremote.git
cd pcremote
```

* *Step 3:* Build and install:
Finally, enter the following commands to build and install PCRemote:

```sh
go build
sudo mv pcremote /usr/local/bin
```

That is it! You've successfully installed PCRemote.

### Starting and using

Now, if you have done everything correctly, you should be able to launch PCRemote with the `pcremote` command. So please do so.

When you launch PCRemote, it will ask you to choose you to select the network interface you want to use. It should look something like this:

![Interface chooser screenshot](screenshots/interface-chooser.png)

Select the one that most resembles a wireless network, probably the one with a 'w' in it (e.g. wlan*n*). In my case, it is called wlp1s0.

After you selected the interface, a QR code, a PIN code, and a URL will appear

![QR code screenshot](screenshots/qr-code.png)

You have two options now: you either scan the QR code, or manually enter the URL written above the QR code.

Whichever you have done, you should see a screen like this on the device that you've scanned the QR code on:

![Phone PIN-code screen](screenshots/phone-screenshot-pin.png)

Enter the PIN code displayed under the QR code on the terminal, and you can use the program! Easy!

If you want to connect multiple devices, keep in mind that a new PIN code is generated for every connection.

When you want to stop the server, simply press Control-C or close the terminal window.

## Credits

### Directly involved in the project
* almaember - coding and basically everything

### Indirectly helped
* Robot-Go devs - that library was useful
* Mark Percival - he made the QR code generation library
* Gorilla/Websocket devs - for making the WebSocket library this project is based on
