// Copyright 2021 almaember

// This file is part of PCRemote.
//
// PCRemote is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PCRemote is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PCRemote.  If not, see <https://www.gnu.org/licenses/>.

package server

import (
	"log"
	"net/http"

	"github.com/gorilla/websocket"

	"gitlab.com/almaember/pcremote/client"
)

func checkWSOrigin(r *http.Request) bool {
	return true // fuck it, this works. I don't care anymore
}

var upgrader = websocket.Upgrader{
	CheckOrigin: checkWSOrigin}

func webSocketEndpoint(w http.ResponseWriter, r *http.Request) {
	c, err := upgrader.Upgrade(w, r, nil)

	if err != nil {
		log.Println("error: ", err)
		return
	}
	defer c.Close()

	for {
		mt, message, err := c.ReadMessage()

		if err != nil {
			return
		}

		if mt != websocket.TextMessage {
			log.Println("error: received binary message")
			continue
		}

		msg := string(message)

		processMessage(msg, c)
	}
}

func ServerMain() {
	log.Println("PIN code: " + currentPinCode)

	serveFiles()
	http.HandleFunc("/socket", webSocketEndpoint)

	log.Fatal(http.ListenAndServe("0.0.0.0:3467", nil))
}

// I couldn't get FileServer to work, so enjoy
// I might fix this, I might not. After all, it works
func serveFiles() {
	// serve index.html
	http.HandleFunc("/index.html", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("content-type", "text/html")
		file, _ := client.ClientFiles.ReadFile("html/index.html")
		w.Write(file)
	})

	// serve remote.css
	http.HandleFunc("/remote.css", func(w http.ResponseWriter, r *http.Request) {
		file, _ := client.ClientFiles.ReadFile("html/remote.css")
		w.Header().Set("content-type", "text/css")
		w.Write(file)
	})

	// serve remote.js
	http.HandleFunc("/remote.js", func(w http.ResponseWriter, r *http.Request) {
		file, _ := client.ClientFiles.ReadFile("html/remote.js")
		w.Header().Set("content-type", "text/javascript")
		w.Write(file)
	})
}
