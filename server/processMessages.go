// Copyright 2021 almaember

// This file is part of PCRemote.
//
// PCRemote is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PCRemote is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PCRemote.  If not, see <https://www.gnu.org/licenses/>.

package server

import (
	"strings"

	"github.com/gorilla/websocket"

	"gitlab.com/almaember/pcremote/gui"
)

func processMessage(msg string, c *websocket.Conn) {
	msgSlice := strings.SplitN(msg, " ", 2)

	cmd, arg := msgSlice[0], msgSlice[1]

	switch cmd {
	case "AUTH":
		handleAuth(arg, c)
	case "ACTION":
		go gui.PressKey(arg)
	}
}

func handleAuth(pin string, c *websocket.Conn) {
	if verifyPin(pin) {
		c.WriteMessage(websocket.TextMessage, []byte("AUTH OK"))
	} else {
		c.WriteMessage(websocket.TextMessage, []byte("AUTH ERROR"))
	}
}
