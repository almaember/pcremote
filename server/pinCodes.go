// Copyright 2021 almaember

// This file is part of PCRemote.
//
// PCRemote is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PCRemote is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PCRemote.  If not, see <https://www.gnu.org/licenses/>.

package server

import (
	"log"
	"math/rand"
	"sync"
	"time"
)

var currentPinCode string
var pinCodeMutex sync.Mutex

func init() {
	log.SetPrefix("")
	log.SetFlags(0)

	rand.Seed(time.Now().UnixNano())
	newPin()
}

func newPin() {
	pinCodeMutex.Lock()
	defer pinCodeMutex.Unlock()

	var length = 4
	var runes = []rune("0123456789")

	pinSlice := make([]rune, length)

	for i := range pinSlice {
		pinSlice[i] = runes[rand.Intn(len(runes))]
	}

	previousPin := currentPinCode
	currentPinCode = string(pinSlice)
	if previousPin != "" {
		log.Println("New PIN code: " + currentPinCode)
	}
}

func verifyPin(pin string) bool {
	pinCodeMutex.Lock()
	defer newPin()
	defer pinCodeMutex.Unlock()

	return pin == currentPinCode
}
