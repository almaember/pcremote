// Copyright 2021 almaember

// This file is part of PCRemote.
//
// PCRemote is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PCRemote is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PCRemote.  If not, see <https://www.gnu.org/licenses/>.

//#region WebSocket setup

function getWSUrl() {
    return `ws://${location.host}/socket`
}

const ws = new WebSocket(getWSUrl())

// this variable will indicate if the WebSocket is already usable
var isWSActive = false

ws.onopen = async (evt) => {
    let pin = prompt("PIN code:")
    ws.send(`AUTH ${pin}`)
}

// the only message we should receive
// is the one confirming that
// the PIN code is right
ws.onmessage = async (evt) => {
    let msg = evt.data
    console.log(evt.data)

    if (msg === "AUTH OK") {
        alert("Authentication successful, you may use the remote now")
        isWSActive = true
    } else if(msg.startsWith("AUTH")) {
        alert("An error happened during authentication")
    }
}

ws.onclose = async (evt) => {
    alert("Connection closed")
}

ws.onerror = async (evt) => {
    alert("Something went wrong :(\nError: " + evt.data)
}

//#endregion

//#region Event handlers

// button event handlers
document.getElementById("btn-left") .addEventListener("click", async () => onButtonPressed("left"))   // left
document.getElementById("btn-right").addEventListener("click", async () => onButtonPressed("right"))  // right
document.getElementById("btn-up")   .addEventListener("click", async () => onButtonPressed("up"))     // up
document.getElementById("btn-down") .addEventListener("click", async () => onButtonPressed("down"))   // down

document.getElementById("btn-space").addEventListener("click", () => onButtonPressed("space"))        // space

async function onButtonPressed(button) {
    if(!isWSActive) {
        return
    }

    // send the event through the websocket connection:
    ws.send(`ACTION ${button.toUpperCase()}`)
}
//#endregion