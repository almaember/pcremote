// Copyright 2021 almaember

// This file is part of PCRemote.
//
// PCRemote is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PCRemote is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PCRemote.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"fmt"

	"gitlab.com/almaember/pcremote/server"
)

func main() {
	// print GPL startup notice
	printGPLNotice()

	ipAddress := getIPAddress()
	fmt.Printf("Go to \"http://%s:3467/index.html\", or scan the QR code:\n", ipAddress)
	displayQRCode(ipAddress)

	server.ServerMain()
}

func printGPLNotice() {
	fmt.Println("PCRemote  Copyright (C) 2021  almaember\n" +
		"This program comes with ABSOLUTELY NO WARRANTY;\nfor details see the COPYING file in the source code.\n" +
		"This is free software, and you are welcome to redistribute it\n" +
		"under certain conditions; see the COPYING file in the source code for details.\n\n")
}
