module gitlab.com/almaember/pcremote

go 1.16

require (
	github.com/go-vgo/robotgo v0.93.1 // indirect
	github.com/gorilla/websocket v1.4.2
	github.com/mdp/qrterminal/v3 v3.0.0
)
